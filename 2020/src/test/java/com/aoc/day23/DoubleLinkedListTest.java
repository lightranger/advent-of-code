package com.aoc.day23;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;


class DoubleLinkedListTest {

    DoubleLinkedList<Long> doubleLinkedList = new DoubleLinkedList<>();

    @BeforeEach
    public void setup(){

    }

    public void shouldAdd() {
        doubleLinkedList.add(1L);
        doubleLinkedList.add(2L);
        doubleLinkedList.add(3L);
        doubleLinkedList.add(4L);
        doubleLinkedList.add(5L);
    }

    @Test
    public void shouldProcess() {
        printList();
    }

    @Test
    void removeElementsAfter() {
        doubleLinkedList.add(1L);
        doubleLinkedList.add(2L);
        doubleLinkedList.add(3L);
        doubleLinkedList.add(4L);
        doubleLinkedList.add(5L);

        int numberOfRemovedElements = 2;
        List<Long> elements = doubleLinkedList.removeElementsAfter(2L, numberOfRemovedElements);
        Assertions.assertEquals(numberOfRemovedElements, elements.size());
        printList();

    }

    @Test
    void insertAfter() {
        doubleLinkedList.add(1L);
        doubleLinkedList.add(2L);
        doubleLinkedList.add(3L);
        doubleLinkedList.add(4L);
        doubleLinkedList.add(5L);

        doubleLinkedList.insertAfter(2L, 15L,16L, 17L);
        printList();

        doubleLinkedList.insertAfter(5L, 81L,82L);

        printList();

    }

    @Test
    void size() {
        doubleLinkedList.add(1L);
        doubleLinkedList.add(2L);
        doubleLinkedList.add(3L);

        Assertions.assertEquals(3, doubleLinkedList.size());

    }

    @Test
    void first() {
        doubleLinkedList.add(1L);
        doubleLinkedList.add(2L);
        doubleLinkedList.add(3L);

        Assertions.assertEquals(1L, (long)doubleLinkedList.getFirst());

    }

    private void printList() {
        doubleLinkedList.processItems(obj -> System.out.print(obj+ " "));
        System.out.println();
    }
}