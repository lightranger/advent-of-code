package com.aoc.day19;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class MonsterMessages {


    private Map<Integer, List<List<String>>> map = new HashMap<>();
    private Map<Integer, Boolean> mapParsedElement = new HashMap<>();


    private List<String> possibleMatches = new ArrayList<>();

    public void solve() throws IOException {
        readInput();

        List<List<String>> allCombinations = getAllCombinations(0);

        Set<String> validCombinations = new HashSet<>();

        for (List<String> allCombination : allCombinations) {
            validCombinations.add(String.join("", allCombination));
        }

        int k = 0;
        for (String possibleMatch : possibleMatches) {
            if(validCombinations.contains(possibleMatch)) {
                k++;
            }
        }


        System.out.println(k);


    }

    private List<List<String>> getAllCombinations(int index) {
        List<List<String>> list = map.get(index);
        if(mapParsedElement.get(index)) {
            return list;
        }
        int removeindex = -5;

        while(removeindex != -1) {
            List<List<String>> newElements = new ArrayList<>();
            removeindex = -1;

            for (int i = 0; i < list.size(); i++) {
                List<String> listElement = list.get(i);

                String elementToRemove = null;
                int indexOfElementToRemove = -1;

                for (int j = 0; j < listElement.size(); j++) {
                    String element = listElement.get(j);
                    if (StringUtils.isNumeric(element)) {
                        elementToRemove = element;
                        indexOfElementToRemove = j;
                        break;
                    }
                }


                if(elementToRemove != null) {
                    //set.remove(elementToRemove);
                    List<List<String>> newElementsFound = getAllCombinations(Integer.parseInt(elementToRemove));

                    for(List<String> elementsFound : newElementsFound) {
                        List<String> builtList = new ArrayList<>();
                        builtList.addAll(listElement.subList(0, indexOfElementToRemove));
                        builtList.addAll(elementsFound);
                        if(indexOfElementToRemove+1 <= listElement.size()) {
                            builtList.addAll(listElement.subList(indexOfElementToRemove + 1, listElement.size()));
                        }
                        newElements.add(builtList);
                    }


                    removeindex = i;
                    break;
                }

            }

            if(removeindex != -1) {
                list.remove(removeindex);
                list.addAll(newElements);

            }

        }


        return list;
    }

    private void readInput() throws IOException {
        List<String> lines = Files.readAllLines(Paths.get(getClass().getClassLoader().getResource("day19/input2.txt").getFile()));
        int i = 0;

        while (!lines.get(i).isEmpty()) {
            String line = lines.get(i);

            String[] split = line.split(":");
            int key = Integer.parseInt(split[0]);
            List<List<String>> list = getList(split[1]);
            map.put(key, list);
            mapParsedElement.put(key, false);
            i++;
        }

        for(i++; i < lines.size() ; i++) {
            possibleMatches.add(lines.get(i));
        }
    }

    private List<List<String>> getList(String line) {
        List<List<String>> list = new ArrayList<>();
        String[] split = line.split("\\|");

        for (String elements : split) {
            String[] s = elements.substring(1).split(" ");
            if(s.length == 1) {
                if(s[0].contains("\"")) {
                    s = new String[] {String.valueOf(s[0].charAt(1))};
                }
            }
            list.add(Arrays.asList(s));
        }

        return list;
    }
}
