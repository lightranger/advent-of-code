package com.aoc.day4.validators;

public class PidValidator {

    public boolean isValid(String value) {
        return value.length() == 9 && value.chars().allMatch( Character::isDigit );
    }
}
