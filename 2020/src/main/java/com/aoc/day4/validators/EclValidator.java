package com.aoc.day4.validators;

import java.util.List;

public class EclValidator {
    private static List<String> VALID_VALUE = List.of("amb", "blu", "brn", "gry", "grn", "hzl", "oth");

    public boolean isValid(String value) {
        return VALID_VALUE.contains(value);
    }
}
