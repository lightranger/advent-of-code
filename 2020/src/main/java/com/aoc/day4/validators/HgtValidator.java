package com.aoc.day4.validators;

public class HgtValidator {

    public boolean isValid(String value) {
        if(value.endsWith("cm")) {
            int number = getNumber(value);
            return number >= 150 && number <= 193;
        } else if(value.endsWith("in")) {
            int number = getNumber(value);
            return number >= 59 && number <= 76;
        }
        return false;
    }

    private int getNumber(String value) {
        return Integer.parseInt(value.substring(0, value.length() - 2));
    }

}
