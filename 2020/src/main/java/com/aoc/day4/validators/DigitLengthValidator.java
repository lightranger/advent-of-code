package com.aoc.day4.validators;

public class DigitLengthValidator {
    public boolean isValid(String value, int min, int max) {
        int number = Integer.parseInt(value);

        return value.length() == 4 && number >= min && number <= max;
    }
}
