package com.aoc.day4.validators;

import java.util.List;

import static java.lang.Character.isDigit;

public class HclValidator {
    private static List<Character> VALID_LETTERS = List.of('a', 'b','c','d','e','f');

    public boolean isValid(String value) {
        return value.startsWith("#") && endsWith(value);
    }

    private boolean endsWith(String value) {
        for(int i = 1; i < value.length(); i++) {
            char charAt = value.charAt(i);
            if(!isDigit(charAt) && !isValidLetter(charAt)) {
                return false;
            }
        }
        return true;
    }

    private boolean isValidLetter(char charAt) {
        return VALID_LETTERS.contains(charAt);
    }
}
