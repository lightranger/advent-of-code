package com.aoc.day4;

import com.aoc.day4.validators.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

public class PassportValidator {
    public static void main(String[] args) throws IOException {
        try (
                Stream<String> stream = Files.lines(Paths.get(PassportValidator.class.getClassLoader().getResource("input.txt").getFile()))) {
                stream.forEach(PassportValidator::processLine);
        }

        System.out.println("Passports: " + VALID_PASSPORT_COUNT);
    }

    enum PassportField {
        BYR , IYR , EYR , HGT , HCL , ECL , PID , CID
    }

    private static Set<PassportField> mandatoryFields = Set.of(PassportField.BYR, PassportField.IYR, PassportField.EYR, PassportField.HGT, PassportField.HCL,
                                                PassportField.ECL,PassportField.PID);

    private static Set<PassportField> foundFields = new HashSet<>();

    private static int VALID_PASSPORT_COUNT = 0;
    private static int TOTAL_PASSPORT_COUNT = 0;

    private static void processLine(String line) {
        if(line.isEmpty()) {
            TOTAL_PASSPORT_COUNT++;
            if(foundFields.containsAll(mandatoryFields)) {
                VALID_PASSPORT_COUNT++;
                System.out.println(TOTAL_PASSPORT_COUNT);
            }

            foundFields = new HashSet<>();
            System.out.println(line);
            return;
        }
        System.out.println(line);

        String[] fields = line.split(" ");
        for (String field : fields) {
            String[] keyValue = field.split(":");
            PassportField key = PassportField.valueOf(keyValue[0].toUpperCase());
            String value = keyValue[1];

            if(isValid(key, value)) {
                foundFields.add(key);
            }
        }

    }

    private static boolean isValid(PassportField key, String value) {
        return switch (key) {
            case BYR -> new DigitLengthValidator().isValid(value, 1920, 2002);
            case IYR -> new DigitLengthValidator().isValid(value, 2010, 2020);
            case EYR -> new DigitLengthValidator().isValid(value, 2020, 2030);
            case HGT -> new HgtValidator().isValid(value);
            case HCL -> new HclValidator().isValid(value);
            case ECL -> new EclValidator().isValid(value);
            case PID -> new PidValidator().isValid(value);
            case CID -> true;
        };
    }

}
