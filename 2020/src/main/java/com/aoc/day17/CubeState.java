package com.aoc.day17;

public enum CubeState {
    ACTIVE('#'), INACTIVE('.');

    private final char symbol;

    CubeState(char symbol) {
        this.symbol = symbol;
    }

    public char getSymbol() {
        return symbol;
    }
}
