package com.aoc.day17;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.BiConsumer;

public abstract class AbstractConwayCubes {
    private static final int MAXIMUM_ITERATIONS = 6;
    protected CubeHolder cubeHolder;
    protected CubeHolder auxCubeHolder;
    protected int activeCells = 0;

    public AbstractConwayCubes(CubeHolder cubeHolder, CubeHolder auxCubeHolder) {
        this.cubeHolder = cubeHolder;
        this.auxCubeHolder = auxCubeHolder;
    }

    public void solve() throws IOException {
        int dimension = initCube();

        int i = 0;

        while(i++ < MAXIMUM_ITERATIONS) {
            System.out.println("Iteration: " + i);
            dimension += 2;
            iterateCube(dimension, this::verifyNeighbours, cubeHolder);
            cubeHolder = auxCubeHolder;
            auxCubeHolder = createCubeHolder();
            //printCube(dimension, cubeHolder);
        };


        iterateCube(dimension, this::countActiveCells, cubeHolder);
        System.out.println(activeCells);
    }

    abstract protected CubeHolder createCubeHolder();

    protected void countActiveCells(Point point, CubeState cubeState) {
        if(cubeState == CubeState.ACTIVE) {
            activeCells++;
        }
    }

    protected void verifyNeighbours(Point point, CubeState cubeState) {
        switch (cubeState) {
            case ACTIVE -> verifyNeighboursOfActiveCube(point);
            case INACTIVE -> verifyNeighboursOfInactiveCube(point);
        }
    }


    protected void verifyNeighboursOfActiveCube(Point point) {
        int activeNeighbours = getActiveNeighbours(point);
        if (activeNeighbours == 2 || activeNeighbours == 3) {
            auxCubeHolder.addElement(point, CubeState.ACTIVE);
        } else {
            auxCubeHolder.addElement(point, CubeState.INACTIVE);
        }
    }

    protected void verifyNeighboursOfInactiveCube(Point point) {
        int activeNeighbours = getActiveNeighbours(point);
        if (activeNeighbours == 3) {
            auxCubeHolder.addElement(point, CubeState.ACTIVE);
        } else {
            auxCubeHolder.addElement(point, CubeState.INACTIVE);
        }
    }

    abstract int getActiveNeighbours(Point point);
    abstract void iterateCube(int dimension, BiConsumer<Point, CubeState> consumer, CubeHolder cubeHolder);
    abstract void printCube(int dimension, CubeHolder cubeHolder);

    protected int initCube() throws IOException {
        List<String> lines = Files.readAllLines(Paths.get(getClass().getClassLoader().getResource("day17/input.txt").getFile()));
        int z = 0;
        int w = 0;
        int offset = lines.size() / 2;
        for (int x = 0; x < lines.size(); x++) {
            String line = lines.get(x);

            char[] charArray = line.toCharArray();
            for (int y = 0; y < charArray.length; y++) {
                char character = charArray[y];
                CubeState cubeState = switch (character) {
                    case '#' -> CubeState.ACTIVE;
                    case '.' -> CubeState.INACTIVE;
                    default -> throw new IllegalStateException("Unexpected value: " + y);
                };
                cubeHolder.addElement(new Point(x - offset, y - offset, z, w), cubeState);
            }
        }
        return lines.size();
    }

}
