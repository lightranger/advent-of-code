package com.aoc.day17;

public class Point {
    int x;
    int y;
    int z;

    public Point(int x, int y, int z, int w) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
    }

    int w;

    public Point(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = 0;
    }
}
