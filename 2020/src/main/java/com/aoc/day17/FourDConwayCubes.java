package com.aoc.day17;

import java.util.Optional;
import java.util.function.BiConsumer;

public class FourDConwayCubes extends AbstractConwayCubes{

    public FourDConwayCubes() {
        super(new FourDCubeHolder(), new FourDCubeHolder());
    }

    @Override
    protected CubeHolder createCubeHolder() {
        return new FourDCubeHolder();
    }

    @Override
    protected int getActiveNeighbours(Point point) {
        int sum = 0;

        for (int i = point.x - 1; i <= point.x + 1; i++) {
            for (int j = point.y - 1; j <= point.y + 1; j++) {
                for (int k = point.z - 1; k <= point.z + 1; k++) {
                    for (int l = point.w - 1; l <= point.w + 1; l++) {
                        if (i == point.x && j == point.y && k == point.z && l == point.w) {
                            continue;
                        }
                        CubeState state = cubeHolder.getState(new Point(i, j, k, l)).orElse(CubeState.INACTIVE);
                        if (state == CubeState.ACTIVE) {
                            //System.out.printf("Active neighbour x: {%d}, y: {%d}, z: {%d}%n",i, j, k);
                            sum++;
                        }
                    }
                }
            }
        }
        return sum;
    }

    @Override
    public void iterateCube(int dimension, BiConsumer<Point, CubeState> consumer, CubeHolder cubeHolder) {
        for (int w = -(dimension-2) / 2; w <= (dimension-2) / 2; w++) {
            for (int z = -(dimension - 2) / 2; z <= (dimension - 2) / 2; z++) {
                for (int x = -dimension / 2; x <= dimension / 2; x++) {
                    for (int y = -dimension / 2; y <= dimension / 2; y++) {
                        Point point = new Point(x, y, z, w);
                        Optional<CubeState> cubeState = cubeHolder.getState(point);
                        if (cubeState.isPresent()) {
                            consumer.accept(point, cubeState.get());
                        } else {
                            consumer.accept(point, CubeState.INACTIVE);
                        }
                    }
                }
            }
        }
    }

    @Override
    protected void printCube(int dimension, CubeHolder cubeHolder) {
        for (int z = -(dimension-2) / 2; z <= (dimension-2) / 2; z++) {
            System.out.println("z = " + z);
            for (int x = -dimension / 2; x <= dimension / 2; x++) {
                for (int y = -dimension / 2; y <= dimension / 2; y++) {
                    Optional<CubeState> cubeState = cubeHolder.getState(new Point(x, y, z));
                    if (cubeState.isPresent()) {
                        System.out.print(cubeState.get().getSymbol());
                    } else {
                        System.out.print(CubeState.INACTIVE.getSymbol());
                    }
                }
                System.out.println();
            }
        }
        System.out.println();
    }


}
