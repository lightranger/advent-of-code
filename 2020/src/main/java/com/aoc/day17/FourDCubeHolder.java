package com.aoc.day17;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static java.util.Objects.nonNull;

public class FourDCubeHolder implements CubeHolder{
    private Map<Integer,Map<Integer, Map<Integer, Map<Integer, CubeState>>>> wzyx;

    public FourDCubeHolder() {
        wzyx = new HashMap<>();
    }

    public void addElement(Point point, CubeState cubeState) {
        var zyx = wzyx.computeIfAbsent(point.w, k -> new HashMap<>());
        var yx = zyx.computeIfAbsent(point.z, k -> new HashMap<>());
        var x = yx.computeIfAbsent(point.y, k -> new HashMap<>());
        x.put(point.x, cubeState);
    }

    public Optional<CubeState> getState(Point point) {
        var zyx = wzyx.get(point.w);
        if(nonNull(zyx)) {
            var yx = zyx.get(point.z);
            if (nonNull(yx)) {
                var x = yx.get(point.y);
                if (nonNull(x)) {
                    return Optional.ofNullable(x.get(point.x));
                }
            }
        }
        return Optional.empty();
    }
}
