package com.aoc.day17;

import java.util.Optional;


public interface CubeHolder {
    void addElement(Point point, CubeState cubeState);
    Optional<CubeState> getState(Point point) ;
}
