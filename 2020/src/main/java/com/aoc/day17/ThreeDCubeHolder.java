package com.aoc.day17;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static java.util.Objects.nonNull;

public class ThreeDCubeHolder implements CubeHolder{
    private static final int OFFSET_COORDINATE = 0;

    private Map<Integer, Map<Integer, Map<Integer, CubeState>>> zyx;

    public ThreeDCubeHolder() {
        zyx = new HashMap<>();
    }

    public void addElement(Point point, CubeState cubeState) {
        var yx = zyx.computeIfAbsent(point.z+OFFSET_COORDINATE, k -> new HashMap<>());
        var x = yx.computeIfAbsent(point.y+OFFSET_COORDINATE, k -> new HashMap<>());
        x.put(point.x+OFFSET_COORDINATE, cubeState);
    }

    public Optional<CubeState> getState(Point point) {
        var yx = zyx.get(point.z+OFFSET_COORDINATE);
        if(nonNull(yx)) {
            var x = yx.get(point.y+OFFSET_COORDINATE);
            if(nonNull(x)) {
                return Optional.ofNullable(x.get(point.x+OFFSET_COORDINATE));
            }
        }
        return Optional.empty();
    }
}
