package com.aoc.day17.three;

import com.aoc.day17.CubeState;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static java.util.Objects.nonNull;

public class CubeHolder {
    private static final int OFFSET_COORDINATE = 0;

    private Map<Integer, Map<Integer, Map<Integer, CubeState>>> zyx;

    public CubeHolder() {
        zyx = new HashMap<>();
    }

    public void addElement(int xPos, int yPos, int zPos, CubeState cubeState) {
        var yx = zyx.computeIfAbsent(zPos+OFFSET_COORDINATE, k -> new HashMap<>());
        var x = yx.computeIfAbsent(yPos+OFFSET_COORDINATE, k -> new HashMap<>());
        x.put(xPos+OFFSET_COORDINATE, cubeState);
    }

    public Optional<CubeState> getState(int xPos, int yPos, int zPos) {
        var yx = zyx.get(zPos+OFFSET_COORDINATE);
        if(nonNull(yx)) {
            var x = yx.get(yPos+OFFSET_COORDINATE);
            if(nonNull(x)) {
                return Optional.ofNullable(x.get(xPos+OFFSET_COORDINATE));
            }
        }
        return Optional.empty();
    }
}
