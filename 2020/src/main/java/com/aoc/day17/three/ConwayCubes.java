package com.aoc.day17.three;

import com.aoc.day17.CubeState;
import com.aoc.day17.QuadConsumer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

public class ConwayCubes {

    private CubeHolder cubeHolder = new CubeHolder();
    private CubeHolder auxCubeHolder = new CubeHolder();
    private int activeCells = 0;
    public static final int MAXIMUM_ITERATIONS = 6;

    public void solve() throws IOException {
        int dimension = initCube();

        int i = 0;

        while(i++ < MAXIMUM_ITERATIONS) {
            System.out.println("Iteration: " + i);
            dimension += 2;
            iterateCube(dimension, this::verifyNeighbours, cubeHolder);
            cubeHolder = auxCubeHolder;
            auxCubeHolder = new CubeHolder();
            //printCube(dimension, cubeHolder);
        };


        iterateCube(dimension, this::countActiveCells, cubeHolder);
        System.out.println(activeCells);
    }

    private void countActiveCells(Integer x, Integer y, Integer z, CubeState cubeState) {
        if(cubeState == CubeState.ACTIVE) {
            activeCells++;
        }
    }


    private void verifyNeighbours(int x, int y, int z, CubeState cubeState) {
        //System.out.println(String.format("Verifying x: {%d}, y: {%d}, z: {%d}",x, y, z));
        switch (cubeState) {
            case ACTIVE -> verifyNeighboursOfActiveCube(x, y, z);
            case INACTIVE -> verifyNeighboursOfInactiveCube(x, y, z);
        }
    }

    private void verifyNeighboursOfActiveCube(int x, int y, int z) {
        int activeNeighbours = getActiveNeighbours(x, y, z);
        if (activeNeighbours == 2 || activeNeighbours == 3) {
            auxCubeHolder.addElement(x, y, z, CubeState.ACTIVE);
        } else {
            auxCubeHolder.addElement(x, y, z, CubeState.INACTIVE);
        }
    }

    private void verifyNeighboursOfInactiveCube(int x, int y, int z) {
        int activeNeighbours = getActiveNeighbours(x, y, z);
        //System.out.printf("Cube x: {%d}, y: {%d}, z: {%d} has %d active neighbours%n",x, y, z, activeNeighbours);
        if (activeNeighbours == 3) {
            auxCubeHolder.addElement(x, y, z, CubeState.ACTIVE);
        } else {
            auxCubeHolder.addElement(x, y, z, CubeState.INACTIVE);
        }
    }

    private int getActiveNeighbours(int x, int y, int z) {
        int sum = 0;

        for (int i = x - 1; i <= x + 1; i++) {
            for (int j = y - 1; j <= y + 1; j++) {
                for (int k = z - 1; k <= z + 1; k++) {
                    if(i==x && j==y && k==z) {
                        continue;
                    }
                    CubeState state = cubeHolder.getState(i, j, k).orElse(CubeState.INACTIVE);
                    if (state == CubeState.ACTIVE) {
                        //System.out.printf("Active neighbour x: {%d}, y: {%d}, z: {%d}%n",i, j, k);
                        sum++;
                    }
                }
            }
        }
        return sum;
    }

    private void printCube(int dimension, CubeHolder cubeHolder) {
        for (int z = -(dimension-2) / 2; z <= (dimension-2) / 2; z++) {
            System.out.println("z = " + z);
            for (int x = -dimension / 2; x <= dimension / 2; x++) {
                for (int y = -dimension / 2; y <= dimension / 2; y++) {
                    Optional<CubeState> cubeState = cubeHolder.getState(x, y, z);
                    if (cubeState.isPresent()) {
                        System.out.print(cubeState.get().getSymbol());
                    } else {
                        System.out.print(CubeState.INACTIVE.getSymbol());
                    }
                }
                System.out.println();
            }
        }
        System.out.println();
    }


    public void iterateCube(int dimension, QuadConsumer<Integer, Integer, Integer, CubeState> consumer, CubeHolder cubeHolder) {

        for (int z = -(dimension-2) / 2; z <= (dimension-2) / 2; z++) {
            for (int x = -dimension / 2; x <= dimension / 2; x++) {
                for (int y = -dimension / 2; y <= dimension / 2; y++) {
                    Optional<CubeState> cubeState = cubeHolder.getState(x, y, z);
                    if (cubeState.isPresent()) {
                        consumer.accept(x, y, z, cubeState.get());
                    } else {
                        consumer.accept(x, y, z, CubeState.INACTIVE);
                    }
                }
            }
        }
    }

    private int initCube() throws IOException {
        List<String> lines = Files.readAllLines(Paths.get(getClass().getClassLoader().getResource("day17/input.txt").getFile()));
        int z = 0;
        int offset = lines.size() / 2;
        for (int x = 0; x < lines.size(); x++) {
            String line = lines.get(x);

            char[] charArray = line.toCharArray();
            for (int y = 0; y < charArray.length; y++) {
                char character = charArray[y];
                CubeState cubeState = switch (character) {
                    case '#' -> CubeState.ACTIVE;
                    case '.' -> CubeState.INACTIVE;
                    default -> throw new IllegalStateException("Unexpected value: " + y);
                };
                cubeHolder.addElement(x - offset, y - offset, z, cubeState);
            }
        }
        return lines.size();
    }


}
