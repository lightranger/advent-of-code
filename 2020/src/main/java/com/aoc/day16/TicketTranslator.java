package com.aoc.day16;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TicketTranslator {
    private static final Pattern EXTRACT_DETAILS_PATTERN  = Pattern.compile(".*: (\\d*)-(\\d*) or (\\d*)-(\\d*)");
    private static final Pattern EXTRACT_TICKET_NUMBERS_PATTERN = Pattern.compile("\\d*,\\d*,.*");
    private Set<Integer> detailTickets;


    private List<Integer> nearbyTickets;
    private List<Integer> yourTickets;
    private Set<Integer> erroredTickets;


    public void solve() throws IOException {
        List<String> lines = Files.readAllLines(Paths.get(getClass().getClassLoader().getResource("day16/input.txt").getFile()));
        detailTickets = new HashSet<>();
        yourTickets = new ArrayList<>();
        nearbyTickets = new ArrayList<>();

        readLines(lines);


        int ticketErrorRate = 0;

        erroredTickets = new HashSet<>();

        for (Integer nearbyTicket : nearbyTickets) {
            if(!detailTickets.contains(nearbyTicket)) {
                ticketErrorRate += nearbyTicket;
                erroredTickets.add(nearbyTicket);
            }
        }

        readLinesPart2(lines);

        System.out.println(ticketErrorRate);
    }

    private List<List<Integer>> readLinesPart2(List<String> lines) {
        TicketType ticketType = TicketType.NONE;

        List<Set<Integer>> allRules = new ArrayList<>();
        List<Set<Integer>> departureRules = new ArrayList<>();
        List<List<Integer>> validTickets = new ArrayList<>();

        for (String line : lines) {
            if(line.isBlank()) {
                continue;
            }
            if(line.contains("nearby tickets")) {
                ticketType = TicketType.NEARBY;
            } else {
                if (ticketType == TicketType.NEARBY) {
                    List<Integer> tickets = Stream.of(line.split(",")).map(Integer::parseInt).collect(Collectors.toList());
                    if(tickets.stream().noneMatch(ticket -> erroredTickets.contains(ticket))) {
                        //System.out.println(++k + " " +Arrays.toString(tickets.toArray()));
                        validTickets.add(tickets);
                    }

                } else {
                    Set<Integer> rules = new HashSet<>();
                    Matcher matcher = EXTRACT_DETAILS_PATTERN.matcher(line);
                    if (matcher.find()) {
                        insertTickets(matcher.group(1), matcher.group(2), rules);
                        insertTickets(matcher.group(3), matcher.group(4), rules);
                        if(line.startsWith("departure")) {
                            departureRules.add(rules);
                        }
                        allRules.add(rules);
                    }

                }

            }

        }

        Set<Integer> takenRules = new HashSet<>();
        Map<Integer, List<Integer>> candidates = new HashMap<>();

       // for(int i = 0; i < allRules.size(); i++ ) {
        //validTickets.stream().noneMatch(ticket -> erroredTickets.contains(ticket));
        boolean matches = false;
        for(int columnIndex = 0; columnIndex < validTickets.get(0).size(); columnIndex++) {


            for(int ruleIndex = 0; ruleIndex < allRules.size(); ruleIndex++ ) {
                matches = true;
                for (int rowIndex = 0; rowIndex < validTickets.size(); rowIndex++) {
                    if (!allRules.get(ruleIndex).contains(validTickets.get(rowIndex).get(columnIndex))) {
                        matches = false;
                        break;
                    }
                }
                if (matches) {
                    takenRules.add(ruleIndex);
                    System.out.println(ruleIndex + " " + (yourTickets.get(columnIndex)) + " " + (columnIndex + 1));

                    candidates.computeIfAbsent(columnIndex, k-> new ArrayList<>()).add(ruleIndex);
                    //break;
                }
            }
        }
        System.out.println();

        Map<Integer, Integer> ruleToIndex = new HashMap<>();
        long product = 1L;

        while(ruleToIndex.keySet().size() != allRules.size()) {
            for (Integer index : candidates.keySet()) {
                List<Integer> list = candidates.get(index);
                if(list.size() == 1 ){
                    Integer rule = list.get(0);
                    System.out.println("Found for rule: " + rule + " and: " + yourTickets.get(index));
                    ruleToIndex.put(rule, index);
                    removeRuleFromCandidates(candidates, rule);
                    if(rule <= 5) {
                        product *= yourTickets.get(index);
                    }
                    break;
                }
            }

        }

        System.out.println(product);

        return validTickets;
    }

    public void removeRuleFromCandidates(Map<Integer, List<Integer>> candidates, Integer rule) {
        for (Integer index : candidates.keySet()) {
            List<Integer> list = candidates.get(index);
            list.remove(rule);
        }

    }

    private void readLines(List<String> lines) {
        TicketType ticketType = TicketType.NONE;

        for (String line : lines) {
            if(line.isBlank()) {
                continue;
            }
            if(line.contains("your ticket")) {
                ticketType = TicketType.YOUR;
            } else if(line.contains("nearby tickets")) {
                ticketType = TicketType.NEARBY;
            } else {
                switch (ticketType) {
                    case YOUR -> Stream.of(line.split(",")).map(Integer::parseInt).forEach(yourTickets::add);
                    case NEARBY -> Stream.of(line.split(",")).map(Integer::parseInt).forEach(nearbyTickets::add);
                    case NONE -> extractTicketDetails(line);
                }

            }

        }
    }

    private void extractTicketDetails(String line) {
        Matcher matcher = EXTRACT_DETAILS_PATTERN.matcher(line);
        if (matcher.find()) {
            insertTickets(matcher.group(1), matcher.group(2), detailTickets);
            insertTickets(matcher.group(3), matcher.group(4), detailTickets);
        }
    }

    private void insertTickets(String lowerS, String higherS, Set<Integer> tickets) {
        int lower = Integer.parseInt(lowerS);
        int higher = Integer.parseInt(higherS);
        for(int i = lower; i <= higher; i++) {
            tickets.add(i);
        }
    }

    enum TicketType {
        YOUR, NEARBY, NONE
    }
}
