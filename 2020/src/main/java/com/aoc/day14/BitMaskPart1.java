package com.aoc.day14;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

public class BitMaskPart1 {
    private static final Pattern EXTRACT_MASK_PATTERN = Pattern.compile("mask = (.*)");
    private static final Pattern EXTRACT_ARRAY_PATTERN = Pattern.compile("mem\\[(\\d+)\\] = (\\d+)");

    private long mem[] = new long[80000];

    public void solve() throws IOException {
        List<String> lines = Files.readAllLines(Paths.get(getClass().getClassLoader().getResource("day14/input.txt").getFile()));

        String mask = "";

        for (String line : lines) {
            if(line.startsWith("mask")) {
                mask = getMask(line);
            } else {
                applyMask(line, mask);
            }
        }

        long result = LongStream.of(mem).sum();
        System.out.println(result);

    }

    private String getMask(String line) {
        Matcher matcher = EXTRACT_MASK_PATTERN.matcher(line);

        if (!matcher.find()) {
            throw new IllegalArgumentException();
        }

        return matcher.group(1).trim();
    }

    private void applyMask(String line, String mask) {
        Matcher matcher = EXTRACT_ARRAY_PATTERN.matcher(line);

        if (!matcher.find()) {
            throw new IllegalArgumentException();
        }

        int position = Integer.parseInt(matcher.group(1).trim());
        long value = Long.parseLong(matcher.group(2).trim());

        mem[position] = applyBitOperations(value, mask);
    }

    private Long applyBitOperations(long number, String mask) {

        for(int i = mask.length() - 1; i>=0; i--) {
            number = switch (mask.charAt(i)) {
                case '0' -> modifyBit(number, mask.length()-i-1, 0L);
                case '1' -> modifyBit(number, mask.length()-i-1, 1L);
                case 'X' -> number;
                default -> throw new IllegalStateException("Unexpected value: " + mask.charAt(i));
            };
        }

        return number;
    }

    public static long modifyBit(long number, long position, long newBitValue) {
        long mask = 1L << position;
        return (number & ~mask) | ((newBitValue << position) & mask);
    }
}
