package com.aoc.day14;

import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.LongStream;
import java.util.stream.Stream;

public class BitMaskPart2 {
    private static final Pattern EXTRACT_MASK_PATTERN = Pattern.compile("mask = (.*)");
    private static final Pattern EXTRACT_ARRAY_PATTERN = Pattern.compile("mem\\[(\\d+)\\] = (\\d+)");

    Map<Long, Long> mem = new HashMap<>();

    public void solve() throws IOException {
        List<String> lines = Files.readAllLines(Paths.get(getClass().getClassLoader().getResource("day14/input.txt").getFile()));

        String mask = "";

        for (String line : lines) {
            if(line.startsWith("mask")) {
                mask = getMask(line);
            } else {
                applyMask(line, mask);
            }
        }

        System.out.println(mem.keySet().stream().mapToLong(key -> mem.get(key)).sum());
    }

    private String getMask(String line) {
        Matcher matcher = EXTRACT_MASK_PATTERN.matcher(line);

        if (!matcher.find()) {
            throw new IllegalArgumentException();
        }

        return matcher.group(1).trim();
    }

    private void applyMask(String line, String mask) {
        Matcher matcher = EXTRACT_ARRAY_PATTERN.matcher(line);

        if (!matcher.find()) {
            throw new IllegalArgumentException();
        }

        int position = Integer.parseInt(matcher.group(1).trim());
        long value = Long.parseLong(matcher.group(2).trim());


        List<Long> positions = getPositions(position, mask);
        positions.forEach(p -> mem.put(p, value));
    }

    private List<Long> getPositions(int position, String mask) {
        char[] binaryNumberChars = StringUtils.leftPad(Long.toBinaryString(position),36,'0').toCharArray();
        char[] resultArray = new char[binaryNumberChars.length];

        for(int i = mask.length() - 1; i>=0; i--) {
            resultArray[i] = switch (mask.charAt(i)) {
                case '0' -> binaryNumberChars[i];
                case '1' -> '1';
                case 'X' -> 'X';
                default -> throw new IllegalStateException("Unexpected value: " + mask.charAt(i));
            };
        }
        String result = new String(resultArray);
        long floatingOccurrences = result.chars().filter(ch -> ch == 'X').count();

        List<String> possibleValues = getPossibleValues(floatingOccurrences);

        return possibleValues.stream()
                .map(element -> replaceString(result, element))
                .map(element-> Long.parseLong(element, 2))
                .collect(Collectors.toList());
    }

    List<String> getPossibleValues(long n) {
        if (n == 0) {
            return Collections.emptyList();
        }

        List<String> list = getPossibleValues(--n);
        if(list.isEmpty()) {
            return List.of("0","1");
        }

        List<String> newList = new ArrayList<>();
        for (String element : list) {
            newList.add("0"+element);
        }
        for (String element : list) {
            newList.add("1"+element);
        }
        return newList;
    }

    private String replaceString(String original, String replaceSequence) {
        char[] result = original.toCharArray();
        int k = 0;

        for(int i = 0; i < original.length(); i++) {
            if(original.charAt(i) == 'X'){
                result[i]=replaceSequence.charAt(k++);
            }
        }
        return new String(result);
    }

}
