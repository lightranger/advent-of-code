package com.aoc.day23;

import org.apache.commons.lang3.ArrayUtils;

import java.util.Arrays;
import java.util.stream.Stream;

public class CrabMoverPart2 {
    private static final int NUMBER_OF_MOVES = 10000000;
    private static final int MAX_ELEMENTS_NO = 1000000;
    //    private static final int NUMBER_OF_MOVES = 100;
//    private static final int MAX_ELEMENTS_NO = 9;
    private static final String INPUT = "389125467";
    private int[] pickUpValues;
    private int[] values;
    private int currentIndex;
    private int destinationIndex;

    enum IterationOrder {
        ASC, DESC;
    }

    class Element {
        char number;
    }


    int index = 0;

    public void solve() {

        currentIndex = 0;
        pickUpValues = new int[3];


        values = INPUT.chars().map(el -> el - '0').toArray();

        values = expandArray(values, MAX_ELEMENTS_NO);

        int i = 0;

        while(++i <= NUMBER_OF_MOVES) {

            int currentValue = values[currentIndex];
            //extractPickupValues(); //TODO WEAK SPOT
            currentIndex = ArrayUtils.indexOf(values, currentValue);

            this.destinationIndex = findDestinationIndex(); //TODO WEAK SPOT
            //System.out.println("Pickup values: " + Arrays.toString(pickUpValues) + " destination value: " + values[destinationIndex] + " current value: " + values[currentIndex]);

            currentValue = values[getNextIndex(currentIndex)];
            //addPickupValues(); // TODO WEAK SPOT
            currentIndex = ArrayUtils.indexOf(values, currentValue);
            //System.out.println(i+ ". " + Arrays.toString(values));
            if(i % 10000 == 0)
                System.out.printf("%.4f\n", i/(float)NUMBER_OF_MOVES * 100);
        }

        int soughtIndex = ArrayUtils.indexOf(values, 1);
        int soughtIndexAfter = getNextIndex(soughtIndex);
        System.out.println("Found after 1: " + values[soughtIndexAfter]);
        System.out.println("Found after after 1: " + values[getNextIndex(soughtIndexAfter)]);
    }

    private int[] expandArray(int[] originalArray, int size) {
        int[] newArray = new int[size];
        System.arraycopy(originalArray, 0, newArray, 0, originalArray.length);
        for(int i=originalArray.length; i<size; i++) {
            newArray[i] = i;
        }
        return newArray;

    }

    private int findDestinationIndex() {
        int currentValue = values[currentIndex];

        int value =  switch (currentValue) {
            case 1 -> returnMax();
            case 2 -> ArrayUtils.contains(pickUpValues, 1) ? returnMax() : 1;
            case 3 -> returnCurrentIndexEqualsThree();
            default -> returnMin(currentValue);
        };

        return ArrayUtils.indexOf(values, value);

    }

    private int returnMin(int currentValue) {
        if(!ArrayUtils.contains(pickUpValues, currentValue-1)) {
            return currentValue-1;
        }
        if(!ArrayUtils.contains(pickUpValues, currentValue-2)) {
            return currentValue-2;
        }
        if(!ArrayUtils.contains(pickUpValues, currentValue-3)) {
            return currentValue-3;
        }
        return returnMax();
    }

    private int returnCurrentIndexEqualsThree() {
        if(ArrayUtils.contains(pickUpValues, 1) && ArrayUtils.contains(pickUpValues, 2)) {
            return returnMax();
        } else if(!ArrayUtils.contains(pickUpValues, 2)){
            return 2;
        } else if(!ArrayUtils.contains(pickUpValues, 1)){
            return 1;
        } else {
            return returnMax();
        }
    }

    private int returnMax() {
        if(!ArrayUtils.contains(pickUpValues, MAX_ELEMENTS_NO)) {
            return MAX_ELEMENTS_NO;
        }
        if(!ArrayUtils.contains(pickUpValues, MAX_ELEMENTS_NO-1)) {
            return MAX_ELEMENTS_NO-1;
        }
        if(!ArrayUtils.contains(pickUpValues, MAX_ELEMENTS_NO-2)) {
            return MAX_ELEMENTS_NO-2;
        }
        return MAX_ELEMENTS_NO-3;
    }

    private void extractPickupValues() {
        int firstNextElementIndex = getNextIndex(currentIndex);
        int secondNextElementIndex = getNextIndex(firstNextElementIndex);
        int thirdNextElementIndex = getNextIndex(secondNextElementIndex);
        pickUpValues = new int[] {values[firstNextElementIndex], values[secondNextElementIndex], values[thirdNextElementIndex]};
        values = ArrayUtils.removeAll(values, firstNextElementIndex, secondNextElementIndex, thirdNextElementIndex);

    }

    private void addPickupValues() {
        if(values.length != MAX_ELEMENTS_NO) {
            values = ArrayUtils.insert(destinationIndex+1, values, pickUpValues[0], pickUpValues[1], pickUpValues[2]);
            return;
        }
        values = ArrayUtils.insert(getNextIndex(destinationIndex), values, pickUpValues[0], pickUpValues[1], pickUpValues[2]);
    }


    private int getNextIndex(int index) {
        return getIndex(index, IterationOrder.ASC);
    }

    private int getPreviousIndex(int index) {
        return getIndex(index, IterationOrder.DESC);
    }

    private int getIndex(int index, IterationOrder iterationOrder) {
        return switch (iterationOrder) {
            case ASC -> index + 1 == values.length ? 0 : index + 1;
            case DESC -> index - 1 == -1 ? values.length - 1 : index - 1;
        };
    }

}
