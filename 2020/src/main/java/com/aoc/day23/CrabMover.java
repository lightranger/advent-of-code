package com.aoc.day23;

import org.apache.commons.lang3.ArrayUtils;

import java.util.Arrays;

public class CrabMover {
    private static final int NUMBER_OF_MOVES = 101;
    private static final int MAX_ELEMENTS_NO = 9;
    private static final String INPUT = "389125467";
    private char[] pickUpValues;
    private char[] values;
    private int currentIndex;
    private int destinationIndex;

    enum IterationOrder {
        ASC, DESC;
    }

    class Element {
        char number;
    }


    int index = 0;

    public void solve() {

        currentIndex = 0;
        pickUpValues = new char[3];
        values = INPUT.toCharArray();
        int i = 0;

        while(++i <= NUMBER_OF_MOVES) {
            System.out.println(i+ ". " + Arrays.toString(values));
            char currentValue = values[currentIndex];
            extractPickupValues();
            currentIndex = ArrayUtils.indexOf(values, currentValue);

            this.destinationIndex = findDestinationIndex();
            //System.out.println("Pickup values: " + Arrays.toString(pickUpValues) + " destination value: " + values[destinationIndex] + " current value: " + values[currentIndex]);

            currentValue = values[getNextIndex(currentIndex)];
            addPickupValues();
            currentIndex = ArrayUtils.indexOf(values, currentValue);
        }



    }
    private int findDestinationIndex() {
        int startIndex = currentIndex;
        int max = -1;
        int maxIndex = -1;
        int maxLowerThanCurrent = - 1;
        int maxLowerThanCurrentIndex = - 1;
        for(int i = getPreviousIndex(startIndex); i  != startIndex; i = getPreviousIndex(i)) {
            if(values[i] > max) {
                max = values[i];
                maxIndex = i;
            }
            if(values[i] > maxLowerThanCurrent && values[i] < values[startIndex]) {
            //if(values[i] < values[startIndex] && values[i] < values[startIndex]) {
                maxLowerThanCurrent = values[i];
                maxLowerThanCurrentIndex = i;
               // return i;
            }
        }
        if(maxLowerThanCurrent != -1) {
            return maxLowerThanCurrentIndex;
        }

        return maxIndex;
    }

    private void extractPickupValues() {
        int firstNextElementIndex = getNextIndex(currentIndex);
        int secondNextElementIndex = getNextIndex(firstNextElementIndex);
        int thirdNextElementIndex = getNextIndex(secondNextElementIndex);
        pickUpValues = new char[] {values[firstNextElementIndex], values[secondNextElementIndex], values[thirdNextElementIndex]};
        values = ArrayUtils.removeAll(values, firstNextElementIndex, secondNextElementIndex, thirdNextElementIndex);

    }

    private void addPickupValues() {
        if(values.length != MAX_ELEMENTS_NO) {
            values = ArrayUtils.insert(destinationIndex+1, values, pickUpValues[0], pickUpValues[1], pickUpValues[2]);
            return;
        }
        values = ArrayUtils.insert(getNextIndex(destinationIndex), values, pickUpValues[0], pickUpValues[1], pickUpValues[2]);
    }


    private int getNextIndex(int index) {
        return getIndex(index, IterationOrder.ASC);
    }

    private int getPreviousIndex(int index) {
        return getIndex(index, IterationOrder.DESC);
    }

    private int getIndex(int index, IterationOrder iterationOrder) {
        return switch (iterationOrder) {
            case ASC -> index + 1 == values.length ? 0 : index + 1;
            case DESC -> index - 1 == -1 ? values.length - 1 : index - 1;
        };
    }

}
