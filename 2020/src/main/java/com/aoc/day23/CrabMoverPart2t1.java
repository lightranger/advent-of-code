package com.aoc.day23;

import org.apache.commons.lang3.ArrayUtils;

import java.util.Arrays;
import java.util.List;

public class CrabMoverPart2t1 {
    private static final int NUMBER_OF_MOVES = 10000000;
    private static final long MAX_ELEMENTS_NO = 1000000L;
//    private static final int NUMBER_OF_MOVES = 100;
//    private static final int MAX_ELEMENTS_NO = 9;
    private static final String INPUT = "394618527";
    private Long[] pickUpValues;
    private DoubleLinkedList<Long> values;
    private long currentValue;


    public void solve() {

        values = new DoubleLinkedList<>();

        INPUT.chars().map(el -> el - '0').forEach(value -> values.add((long) value));

        expandList(values, MAX_ELEMENTS_NO);

        currentValue = values.getFirst();
        int i = 0;

        while(++i <= NUMBER_OF_MOVES) {
            pickUpValues = values.removeElementsAfter(currentValue, 3).toArray(new Long[3]);

            long destinationValue = findDestinationValue();

            currentValue = values.getNextAfter(currentValue);

            values.insertAfter(destinationValue, pickUpValues);

            if(i % 10000 == 0)
                System.out.printf("%.4f\n", i/(float)NUMBER_OF_MOVES * 100);
        }

        long nextAfter1 = values.getNextAfter(1L);
        long nextnextAfter1 = values.getNextAfter(nextAfter1);
        System.out.println(nextAfter1 + " " + nextnextAfter1 + " " + nextAfter1 * nextnextAfter1);

    }

    private void expandList(DoubleLinkedList<Long> list, long size) {
        for(long i=list.size()+1; i<=size; i++) {
            list.add(i);
        }
    }

    private long findDestinationValue() {
        long value;
        if (currentValue == 1) {
            value = returnMax();
        } else if (currentValue == 2) {
            value = ArrayUtils.contains(pickUpValues, 1L) ? returnMax() : 1L;
        } else if (currentValue == 3) {
            value = returnCurrentIndexEqualsThree();
        } else {
            value = returnMin(currentValue);
        }

        return value;

    }

    private long returnMin(long currentValue) {
        if(!ArrayUtils.contains(pickUpValues, currentValue-1)) {
            return currentValue-1;
        }
        if(!ArrayUtils.contains(pickUpValues, currentValue-2)) {
            return currentValue-2;
        }
        if(!ArrayUtils.contains(pickUpValues, currentValue-3)) {
            return currentValue-3;
        }
        return returnMax();
    }

    private long returnCurrentIndexEqualsThree() {
        if(ArrayUtils.contains(pickUpValues, 1) && ArrayUtils.contains(pickUpValues, 2L)) {
            return returnMax();
        } else if(!ArrayUtils.contains(pickUpValues, 2L)){
            return 2L;
        } else if(!ArrayUtils.contains(pickUpValues, 1L)){
            return 1L;
        } else {
            return returnMax();
        }
    }

    private long returnMax() {
        if(!ArrayUtils.contains(pickUpValues, MAX_ELEMENTS_NO)) {
            return MAX_ELEMENTS_NO;
        }
        if(!ArrayUtils.contains(pickUpValues, MAX_ELEMENTS_NO-1)) {
            return MAX_ELEMENTS_NO-1L;
        }
        if(!ArrayUtils.contains(pickUpValues, MAX_ELEMENTS_NO-2)) {
            return MAX_ELEMENTS_NO-2L;
        }
        return MAX_ELEMENTS_NO-3L;
    }

}
