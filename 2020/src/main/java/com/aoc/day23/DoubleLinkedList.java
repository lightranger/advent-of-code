package com.aoc.day23;

import java.util.*;
import java.util.function.Consumer;

public class DoubleLinkedList<E> {
    private Map<E, Node<E>> elementsMap = new TreeMap();
    private Node<E> first;
    private Node<E> last;

    private static class Node<E> {
        E item;

        Node<E> previous;
        Node<E> next;
        public Node(E item, Node<E> previous, Node<E> next) {
            this.item = item;
            this.previous = previous;
            this.next = next;
        }

    }

    public void add(E item) {
        Node<E> node = new Node<>(item, last, null);
        elementsMap.put(item, node);
        if(Objects.nonNull(last)) {
            last.next = node;
            node.previous = last;
        }
        if(Objects.isNull(first)) {
            first = node;
        }
        last = node;
        last.next = first;
        first.previous = last;
    }

    public void processItems(Consumer<E> consumer) {
        Node<E> node = first;

        for(int i = 0; i < elementsMap.keySet().size(); i++) {
            consumer.accept(node.item);
            node = node.next;
        }
    }

    public void insertAfter(E item, E... items) {
        Node<E> previous = elementsMap.get(item);

        for (E newItem : items) {
            Node<E> newNode = new Node<>(newItem, previous, previous.next);
            elementsMap.put(newNode.item, newNode);
            newNode.next.previous = newNode;
            previous.next = newNode;
            previous = newNode;

        }

        System.out.print("");
    }

    public E getNextAfter(E currentValue) {
        Node<E> element = elementsMap.get(currentValue);
        if(element == null) {
            throw new IllegalArgumentException();
        }
        return element.next.item;
    }

    public int size() {
        return elementsMap.keySet().size();
    }

    public E getFirst() {
        return first.item;
    }

    public List<E> removeElementsAfter(E item, int numberOfElements) {
        List<E> removedElements = new ArrayList<>();
        final Node<E> node = elementsMap.get(item);
        Node<E> nextNode = node.next;
        for(int i = 0 ; i<numberOfElements; i++) {
            if(first == nextNode) {
                first = nextNode.next;
            }
            elementsMap.remove(nextNode.item);
            removedElements.add(nextNode.item);
            nextNode = nextNode.next;
        }
        node.next = nextNode;
        nextNode.previous = node;
        return removedElements;
    }


}
