package com.aoc.day9;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.LongConsumer;
import java.util.stream.Stream;

public class EncodingError {
    public void solve() throws IOException {

        //readFromFile("day9/input.txt", this::lineProcessor);
        readFromFile("day9/input.txt", this::lineProcessor2);
    }

    static int i = 0;
    private TreeSet<Long> numbers = new TreeSet<>();
    private final static int PREAMBLE_SIZE = 25;
    private Queue<Long> stack = new LinkedList<>();



    private void lineProcessor(Long number) {
        i++;
        if(i <= PREAMBLE_SIZE) {
            numbers.add(number);
            stack.offer(number);
            return;
        }

        Long[] view = numbers.toArray(new Long[PREAMBLE_SIZE]);

        if(!isSumOk(number, view)) {
            System.out.println(number + Arrays.toString(view));
            System.exit(0);
        }

        Long numberToBeRemoved = stack.poll();
        numbers.remove(numberToBeRemoved);

        numbers.add(number);
        stack.offer(number);

    }



    private boolean isSumOk(Long number, Long[] array) {
        int lower = 0;
        int higher = array.length - 1;

        do {
            Long difference =  array[lower] + array[higher] - number;
            if(difference > 0) {
                higher--;
            } else if(difference < 0){
                lower++;
            } else {
                return true;
            }
        } while(lower < higher);
        return false;
    }


    //part 2

    static Queue<Long> numberStack = new LinkedList<>();
    static long sum = 0;
    //private final static int TARGET_SCORE = 127;
    private final static long TARGET_SCORE = 1212510616L;


    private void lineProcessor2(Long number) {
        if(number > TARGET_SCORE) {
            System.out.println("Game Over");
            System.exit(0);
            return;
        }
        if (number + sum < TARGET_SCORE) {
            addNumber(number);
        } else if (number + sum == TARGET_SCORE) {
            checkMatch();
        } else {
            removeNumbers(number);
        }

    }

    private void checkMatch() {
        System.out.println("YAYYYY!!!!!!!  " + Arrays.toString(numberStack.toArray()));
        Long[] numbers = numberStack.toArray(new Long[numberStack.size()]);
        Arrays.sort(numbers);
        System.out.println(numbers[0] + numbers[numbers.length - 1]);
        System.exit(0);
    }

    private void removeNumbers(Long number) {
        boolean repeat = true;
        do {
            Long numberToBeSubtracted = numberStack.poll();
            sum -= numberToBeSubtracted;

            if (number + sum < TARGET_SCORE) {
                addNumber(number);
                repeat = false;
            } else if (number + sum == TARGET_SCORE) {
                checkMatch();
            }

        } while (repeat);
    }

    private void addNumber(Long number) {
        sum += number;
        numberStack.offer(number);
    }


    private void readFromFile(String fileName, LongConsumer lineParser) throws IOException {
        try (
            Stream<String> stream = Files.lines(Paths.get(getClass().getClassLoader().getResource(fileName).getFile()))
        ) {
            stream.mapToLong(Long::parseLong).forEach(lineParser);
        }
    }
}
