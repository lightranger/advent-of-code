package com.aoc.day1;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.Set;
import java.util.function.LongConsumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ExpenseReport {

    public void solve() throws IOException {
        Set<Long> numbers = readFromFile("day1/input.txt");
        Long[] numbersArray = numbers.toArray(new Long[0]);

        for (int i = 0; i < numbersArray.length - 1; i++) {
            for (int j = i + 1; j < numbersArray.length; j++) {
                Long difference = 2020 - numbersArray[i] - numbersArray[j];
                if (numbers.contains(difference)) {
                    System.out.println(difference * numbersArray[i] * numbersArray[j]);
                    break;
                }
            }

        }
    }




    private Set<Long> readFromFile(String fileName) throws IOException {
        try (
                Stream<String> stream = Files.lines(Paths.get(getClass().getClassLoader().getResource(fileName).getFile()))
        ) {
            return stream.mapToLong(Long::parseLong).boxed().collect(Collectors.toSet());
        }
    }
}
