package com.aoc.day12;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class NavigationDirections {
    private int direction = 0;
    private final Point point = new Point(0, 0);


    public void solve() throws IOException {
        List<String> lines = Files.readAllLines(Paths.get(getClass().getClassLoader().getResource("day12/input.txt").getFile()));

        for (String line : lines) {
            char action = line.charAt(0);
            int value = Integer.parseInt(line.substring(1));

            switch (action) {
                case 'N' -> moveNorth(value);
                case 'S' -> moveSouth(value);
                case 'E' -> moveEast(value);
                case 'W' -> moveWest(value);
                case 'L' -> rotateLeft(value);
                case 'R' -> rotateRight(value);
                case 'F' -> moveFront(value);

                default -> throw new IllegalStateException("Unexpected value: " + action);
            }
        }


        System.out.println(Math.abs(point.x) + Math.abs(point.y));
    }

    public void moveNorth(int value) {
        point.x += value;
    }

    public void moveSouth(int value) {
        point.x -= value;
    }

    public void moveEast(int value) {
        point.y += value;
    }

    public void moveWest(int value) {
        point.y -= value;
    }

    public void rotateLeft(int value) {
        direction -= value;
        direction = direction % 360;
        if(direction == -90) {
            direction = 270;
        } else if(direction == -180) {
            direction = 180;
        } else if(direction == -270) {
            direction = 90;
        }
    }

    public void rotateRight(int value) {
        direction += value;
        direction = direction % 360;
    }

    public void moveFront(int value) {
        switch (direction) {
            case 0 -> moveEast(value);
            case 90 -> moveSouth(value);
            case 180 -> moveWest(value);
            case 270 -> moveNorth(value);
            default -> throw new IllegalStateException("Unexpected value: " + direction);
        }
    }
}
