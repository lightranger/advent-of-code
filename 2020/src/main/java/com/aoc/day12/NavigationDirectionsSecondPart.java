package com.aoc.day12;

import java.io.IOException;
import java.lang.invoke.VarHandle;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class NavigationDirectionsSecondPart {
    private final Point ship = new Point(0, 0);
    private final Point waypoint = new Point(10, 1);


    public void solve() throws IOException {
        List<String> lines = Files.readAllLines(Paths.get(getClass().getClassLoader().getResource("day12/input.txt").getFile()));

        for (String line : lines) {
            char action = line.charAt(0);
            int value = Integer.parseInt(line.substring(1));

            switch (action) {
                case 'N' -> moveNorth(value);
                case 'S' -> moveSouth(value);
                case 'E' -> moveEast(value);
                case 'W' -> moveWest(value);
                case 'L' -> rotateLeft(value);
                case 'R' -> rotateRight(value);
                case 'F' -> moveFront(value);

                default -> throw new IllegalStateException("Unexpected value: " + action);
            }
        }


        System.out.println(Math.abs(ship.x) + Math.abs(ship.y));
    }

    public void moveNorth(int value) {
        waypoint.y += value;
    }

    public void moveSouth(int value) {
        waypoint.y -= value;
    }

    public void moveEast(int value) {
        waypoint.x += value;
    }

    public void moveWest(int value) {
        waypoint.x -= value;
    }

    public void rotateLeft(int value) {
        int degrees = value % 360;
        int noOfRotations = degrees / 90;
        while(noOfRotations-- > 0) {
            rotate90AntiClockwise();
        }
    }

    public void rotateRight(int value) {
        int degrees = value % 360;
        int noOfRotations = degrees / 90;
        while(noOfRotations-- > 0) {
            rotate90Clockwise();
        }
    }

    public void rotate90AntiClockwise() {
        int x = waypoint.x;
        int y = waypoint.y;
        waypoint.x = -y;
        waypoint.y = x;
    }

    public void rotate90Clockwise() {
        int x = waypoint.x;
        int y = waypoint.y;
        waypoint.x = y;
        waypoint.y = -x;
    }

    public void moveFront(int value) {
       ship.x += waypoint.x * value;
       ship.y += waypoint.y * value;
    }
}
