package com.aoc.day13;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Schedule {
    public void solve() throws IOException {
        List<String> lines = Files.readAllLines(Paths.get(getClass().getClassLoader().getResource("day13/input.txt").getFile()));

        int earliestTimeOfDeparture = Integer.parseInt(lines.get(0));
        int timeOfDeparture = earliestTimeOfDeparture;

        List<Integer> busIds = Stream.of(lines.get(1).split(","))
                .filter(el -> !el.equals("x"))
                .mapToInt(Integer::parseInt).boxed()
                .collect(Collectors.toList());
        boolean found = false;

        while (!found) {
            for (Integer busId : busIds) {
                if (timeOfDeparture % busId == 0) {
                    System.out.println((timeOfDeparture-earliestTimeOfDeparture)*busId);
                    found = true;
                    break;
                }
            }
            timeOfDeparture++;

        }

    }
}
