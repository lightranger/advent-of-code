package com.aoc.day13;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ScheduleSecondPart {
    static long step;

    public void solve() throws IOException {
        List<String> lines = Files.readAllLines(Paths.get(getClass().getClassLoader().getResource("day13/input.txt").getFile()));

        Integer[] delays = getDelays(lines.get(1).split(","));

        List<Integer> busIds = Stream.of(lines.get(1).split(","))
                .filter(el -> !el.equals("x"))
                .mapToInt(Integer::parseInt).boxed()
                .collect(Collectors.toList());

        Integer[] busIdsArray = busIds.toArray(new Integer[busIds.size()]);

        step = (long) busIds.get(0);

        long i = 0;
        while (i < Long.MAX_VALUE) {
            final long elem = i;
                checkIfBusesAreArrivingAsScheduled(elem, busIdsArray, delays);

            i += step;
        }

        System.out.println("finished");

    }

    int max = 1;

    private void checkIfBusesAreArrivingAsScheduled(long i, Integer[] busIds, Integer[] delays) {
        int j = 0;
        Long sum = 0L;
        while ((i + sum + j) % busIds[j] == 0) {
            j++;
            if (j == busIds.length) {
                System.out.println("Result: " + i);
                System.exit(0);
            } else {
                if(j>max) {
                    step=step*busIds[j-1];
                    max = j;
                }
            }
            sum += delays[j];
        }
    }

    private Integer[] getDelays(String[] busIds) {
        List<Integer> delays = new ArrayList<>();
        int delay = 0;
        for (String busId : busIds) {
            if (busId.equals("x")) {
                delay++;
            } else {
                delays.add(delay);
                delay = 0;
            }

        }
        return delays.toArray(new Integer[0]);

    }

}
