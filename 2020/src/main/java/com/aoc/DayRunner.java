package com.aoc;

import com.aoc.day10.JoltStuff;
import com.aoc.day11.Seats;
import com.aoc.day16.TicketTranslator;
import com.aoc.day18.DifferentMath;
import com.aoc.day19.MonsterMessages;
import com.aoc.day23.CrabMoverPart2;
import com.aoc.day23.CrabMoverPart2t1;

import java.io.IOException;

public class DayRunner {
    public static void main(String[] args) throws IOException {
        //new ExpenseReport().solve(); // day 1
        //new ColoredBags().solve(); // day 7
        //new EncodingError().solve(); // day 9
        //new JoltStuff().solve(); // day 10
        //new Seats().solve(); // day 11
        //new NavigationDirectionsSecondPart().solve(); // day 12
        //new ScheduleSecondPart().solve(); // day 13
        //new BitMaskPart1().solve(); // day 14 part 1
        //new BitMaskPart2().solve(); // day 14 part 2
        //new NumberGame().solve(); // day 15
        //new TicketTranslator().solve(); // day 16
        //new ThreeDConwayCubes().solve(); // day 17 part 1
        //new FourDConwayCubes().solve(); // day 17 part 2
        //new DifferentMath().solve(); // day 18
        new MonsterMessages().solve(); // day 19
        //new CrabMoverPart2t1().solve(); // day 23
        //new CardDoor().solve(); // day 25

    }
}
