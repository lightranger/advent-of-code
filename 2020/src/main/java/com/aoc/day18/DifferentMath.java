package com.aoc.day18;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.Function;

public class DifferentMath {

    public void solve() throws IOException {
        List<String> lines = Files.readAllLines(Paths.get(getClass().getClassLoader().getResource("day18/input.txt").getFile()));


        String expression = "((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2";
        String simpleExpr = "1 + 2 * 3 + 4 * 5 + 6";

        System.out.println(calculateSimple(simpleExpr));

        System.out.println(lines.stream().mapToLong(expression1 -> calculateComplex(expression1, this::calculateSimple)).sum());
        System.out.println(lines.stream().mapToLong(expression1 -> calculateComplex(expression1, this::calculateSimpleComplex)).sum());
    }

    private long calculateComplex(String expression, Function<String, Long> function) {
        int beginIndex = -1;

        while(expression.contains("(")) {
            for (int i = 0; i < expression.length(); i++) {
                if (expression.charAt(i) == '(') {
                    beginIndex = i;
                } else if (expression.charAt(i) == ')') {
                    long result = function.apply(expression.substring(beginIndex + 1, i));
                    expression = expression.substring(0, beginIndex) + result + expression.substring(i + 1);
                    break;
                }
            }
        }

        return function.apply(expression);

    }

    private long calculateSimple(String simpleExpr) {
        long result = 0;
        String[] split = simpleExpr.split(" ");
        Operation operation = Operation.SUM;

        for (String element : split) {
            if(StringUtils.isNumeric(element)) {
                if(result == 0) {
                    result = Long.parseLong(element);
                } else {
                    result = operation.calculate(result, Integer.parseInt(element));
                }
            } else if(element.equals("+")) {
                operation = Operation.SUM;
            } else if(element.equals("*")) {
                operation = Operation.PROD;
            }
        }

        return result;

    }

    private long calculateSimpleComplex(String simpleExpr) {
        long result = 1;
        String[] split = simpleExpr.split("\\*");

        for (String s : split) {
            result *= calculateSimple(s);
        }


        return result;

    }


    enum Operation {
        SUM {
            long calculate(long a,long b) {
                return a+b;
            }
        }, PROD {
            long calculate(long a,long b) {
                return a * b;
            }
        };

        abstract long calculate(long a,long b);
    }

}
