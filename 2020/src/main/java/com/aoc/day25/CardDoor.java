package com.aoc.day25;

public class CardDoor {
    private static final int CARD_PUBLIC_KEY = 19241437;
    //    private static final long CARD_PUBLIC_KEY = 5764801;
    private static final int DOOR_PUBLIC_KEY = 17346587;
//    private static final long DOOR_PUBLIC_KEY = 17807724;


    public void solve() {
        long subjectNumber = 7;

        int cardLoopSize = findLoopSize(subjectNumber, CARD_PUBLIC_KEY);
        int doorLoopSize = findLoopSize(subjectNumber, DOOR_PUBLIC_KEY);
        System.out.println(getEncryptionKey(CARD_PUBLIC_KEY, doorLoopSize));
        System.out.println(getEncryptionKey(DOOR_PUBLIC_KEY, cardLoopSize));
    }

    private int findLoopSize(long subjectNumber, long publicKey) {
        int k = 0;
        long value = 1L;
        while (value != publicKey) {
            k++;
            value = applyIteration(subjectNumber, value);
        }
        return k;
    }

    private long getEncryptionKey(long subjectNumber, int loopSize) {
        long value = 1L;
        for (int i = 0; i < loopSize; i++) {
            value = applyIteration(subjectNumber, value);
        }
        return value;
    }

    private long applyIteration(long subjectNumber, long value) {
        return (value * subjectNumber) % 20201227L;

    }
}
