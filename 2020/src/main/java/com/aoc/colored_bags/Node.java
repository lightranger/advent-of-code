package com.aoc.colored_bags;

import java.util.ArrayList;
import java.util.List;

class Node {
    String color;

    public Node(String color) {
        this.color = color;
    }

    List<Node> inwardNodes = new ArrayList<>();
    List<NodeQuantity> outwardNodes = new ArrayList<>();

    public void addInwardNode(Node node) {
        inwardNodes.add(node);
    }

    public void addOutwardNode(NodeQuantity node) {
        outwardNodes.add(node);
    }
}
