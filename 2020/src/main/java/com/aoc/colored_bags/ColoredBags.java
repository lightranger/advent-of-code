package com.aoc.colored_bags;

import java.io.IOException;
import java.util.*;

public class ColoredBags {
    private Set<String> visitedNodes = new HashSet<>();

    public void solve() throws IOException {
        ColoredBagsInput coloredBagsInput = new ColoredBagsInput();

        //first part
        Node node = coloredBagsInput.getNode("shiny gold");
        computeVisitedNodes(node);
        System.out.println(visitedNodes.size() - 1);

        //second part
        System.out.println(computeQuantity(node));
    }

    private void computeVisitedNodes(Node node) {
        if(visitedNodes.contains(node.color)) {
            return;
        }
        visitedNodes.add(node.color);
        for (Node inwardNode : node.inwardNodes) {
            computeVisitedNodes(inwardNode);
        }

    }

    private int computeQuantity(Node node) {
        if(node.outwardNodes.isEmpty()) {
            return 0;
        }
        int sum = 0;
        for (NodeQuantity outwardNode : node.outwardNodes) {
            int result = computeQuantity(outwardNode.node);
            //System.out.println(node.color + ": adding to sum: " + outwardNode.quantity + " * " + result + "[sum=" + sum + "]");
            sum += outwardNode.quantity * (result + 1);
        }
        return sum;

    }

}
