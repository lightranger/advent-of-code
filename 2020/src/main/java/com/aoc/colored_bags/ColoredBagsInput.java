package com.aoc.colored_bags;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class ColoredBagsInput {
    private static final Pattern EXTRACT_COLOR_AND_SECOND_PART_PATTERN = Pattern.compile("(.*) bags contain (.*)");
    private static final Pattern EXTRACT_QUANTITY_AND_COLOR_PATTERN = Pattern.compile("(\\d)(.*) bag");
    private Map<String, Node> colorMappings;

    public ColoredBagsInput() throws IOException {
        colorMappings = new HashMap<>();
        computeColorMappings();
    }

    public Node getNode(String colorName) {
        return colorMappings.get(colorName);
    }

    private void computeColorMappings() throws IOException {
        readFromFile("colored_bags/input.txt", this::processLine);

    }

    private void readFromFile(String fileName, Consumer<String> lineParser) throws IOException {
        try (
                Stream<String> stream = Files.lines(Paths.get(getClass().getClassLoader().getResource(fileName).getFile()))
        ) {
            stream.forEach(lineParser);
        }
    }


    private void processLine(String line) {
        Matcher matcher = EXTRACT_COLOR_AND_SECOND_PART_PATTERN.matcher(line);

        if (!matcher.find()) {
            return;
        }

        String color = matcher.group(1).trim();
        String secondPart = matcher.group(2);

        computeNodes(secondPart, color);
    }

    private void computeNodes(String secondPart, String parentColor) {
        String[] split = secondPart.split(",");

        for (String s : split) {
            Matcher matcher = EXTRACT_QUANTITY_AND_COLOR_PATTERN.matcher(s);
            if(!matcher.find()) {
                continue;
            }
            String childColor = matcher.group(2).trim();
            int quantity = Integer.parseInt(matcher.group(1));

            Node parentNode = colorMappings.computeIfAbsent(parentColor, el -> new Node(parentColor));
            Node childNode = colorMappings.computeIfAbsent(childColor, el -> new Node(childColor));

            childNode.addInwardNode(parentNode);
            parentNode.addOutwardNode(new NodeQuantity(childNode, quantity));
        }
    }

}
