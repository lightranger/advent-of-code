package com.aoc.colored_bags;

class NodeQuantity {
    Node node;
    int quantity;

    public NodeQuantity(Node node, int quantity) {
        this.node = node;
        this.quantity = quantity;
    }
}
