package com.aoc.day10;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.stream.Stream;

public class JoltStuff {
    private static int differenceOfOne = 0;
    private static int differenceOfThree = 0;

    public void solve() throws IOException {
        long[] numbers = readFromFile("day10/input.txt");

        initSequence();
        //countDifferences(numbers);
        countArrangementsOptimal(numbers);
        //System.out.println(count);
    }
    private void initSequence() {
        sequence[0] = 1;
        sequence[1] = 1;
        sequence[2] = 2;
        for(int i=3;i<256;i++) {
            sequence[i] = sequence[i-1] + sequence[i-2] + sequence[i-3];
        }

    }
    static int[] sequence = new int[256];

    private void countArrangementsOptimal(long[] numbers) {
        long result = 1;
        int consecutiveNumbers = 1;
        boolean wasPreviouslyConsecutive = false;
        for (int i = 0; i < numbers.length - 1; i++) {
            if(numbers[i+1] - numbers[i] == 1) {
                consecutiveNumbers++;
                wasPreviouslyConsecutive = true;
            } else if(wasPreviouslyConsecutive){
                System.out.println("Consecutive numbers: " + consecutiveNumbers);
                result = result * sequence[consecutiveNumbers-1];
                consecutiveNumbers = 1;
                wasPreviouslyConsecutive = false;
            }
        }

        if(wasPreviouslyConsecutive){
            result = result * sequence[consecutiveNumbers-1];
        }
        System.out.println(result);

    }

/*    private void countArrangements(long[] numbers, int position) {
        if(position == numbers.length - 1) {
            //System.out.println(Arrays.toString(numbers));
            count++;
            //System.out.println(count);
        }
      //  int nextPosition = position + 1;
        verifyNextPosition(numbers, position, position+1);
        verifyNextPosition(numbers, position, position+2);
        verifyNextPosition(numbers, position, position+3);
    }

    private void verifyNextPosition(long[] numbers, int position, int nextPosition) {
        if(nextPosition < numbers.length && numbers[nextPosition]-numbers[position] <= 3 ) {
            countArrangements(numbers, nextPosition);
        }
    }*/

    private long getElement(long[] numbers, int position) {
        if(position == 0)
            return 0;
        return numbers[position-1];
    }

    private void countDifferences(long[] numbers) {
        //differenceOfOne++;

        for(int i = 0; i < numbers.length - 1 ; i++) {
            long difference = numbers[i + 1] - numbers[i];
            if(difference == 1) {
                differenceOfOne++;
            } else if(difference == 3) {
                differenceOfThree++;
            }

        }
        differenceOfThree++;

        System.out.println(differenceOfOne * differenceOfThree);
    }

    private long[] readFromFile(String fileName) throws IOException {
        try (
                Stream<String> stream = Files.lines(Paths.get(getClass().getClassLoader().getResource(fileName).getFile()))
        ) {
            return stream.mapToLong(Long::parseLong).sorted().toArray();
        }
    }

}
