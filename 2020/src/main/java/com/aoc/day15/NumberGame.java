package com.aoc.day15;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NumberGame {
    private static final String INPUT = "13,0,10,12,1,5,8";
//    private static final String INPUT = "3,1,2";


    private Map<Integer, List<Integer>> occurrencesPositions = new HashMap<>();
    private Map<Integer, Integer> noOfOccurrences = new HashMap<>();
    private int lastSpokenNumber;
    private int i;

    public void solve() {
        String[] startingNumbers = INPUT.split(",");


        i = 0;
        while (i < startingNumbers.length) {
            int startingNumber = Integer.parseInt(startingNumbers[i]);
            updateLastSpokenNumber(startingNumber);
        }


        for(; i < 30000000;) {
            Integer occurrenceTimes = noOfOccurrences.get(lastSpokenNumber);
            if (occurrenceTimes == 1) {
                updateLastSpokenNumber(0);
            } else if(occurrenceTimes > 1) {
                List<Integer> lastOccurrencesPositions = occurrencesPositions.get(lastSpokenNumber);
                int number = lastOccurrencesPositions.get(lastOccurrencesPositions.size()-1) - lastOccurrencesPositions.get(lastOccurrencesPositions.size()-2);
                updateLastSpokenNumber(number);
            } else {
                updateLastSpokenNumber(0);
            }
            System.out.println("Iteration: " + i + " , last spokenNumber: " +lastSpokenNumber);
        }

    }

    public void updateLastSpokenNumber(int number) {
        lastSpokenNumber = number;
        Integer integer = noOfOccurrences.computeIfAbsent(number, nr -> 0);
        noOfOccurrences.put(lastSpokenNumber, integer+1);
        occurrencesPositions.computeIfAbsent(number, el -> new ArrayList<>()).add(i);
        i++;
    }
}
