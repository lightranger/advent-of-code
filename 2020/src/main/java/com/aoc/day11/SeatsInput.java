package com.aoc.day11;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Stream;

public class SeatsInput {

    private List<List<SeatStatus>> seats = new ArrayList<>();

    public SeatStatus[][] getSeats() throws IOException {
        readFromFile("day11/input.txt", this::processLine);

        return seats.stream()
                .map(arr -> arr.toArray(SeatStatus[]::new))
                .toArray(SeatStatus[][]::new);

    }

    public void processLine(String line) {
        List<SeatStatus> lineSeats = new ArrayList<>();
        for(int i = 0; i<line.length(); i++) {
            SeatStatus seat = switch (line.charAt(i)) {
                case 'L' ->  SeatStatus.EMPTY;
                case '#' -> SeatStatus.OCCUPIED;
                case '.' -> SeatStatus.FLOOR;
                default -> throw new IllegalStateException("Unexpected value: " + line.charAt(i));
            };
            lineSeats.add(seat);
        }
        seats.add(lineSeats);
    }

    private void readFromFile(String fileName, Consumer<String> lineParser) throws IOException {
        try (
                Stream<String> stream = Files.lines(Paths.get(getClass().getClassLoader().getResource(fileName).getFile()))
        ) {
            stream.forEach(lineParser);
        }
    }
}
