package com.aoc.day11;

public enum SeatStatus {
    OCCUPIED("#"), EMPTY("L"), FLOOR(".");

    private String symbol;

    SeatStatus(String symbol) {
        this.symbol = symbol;
    }

    public String getSymbol() {
        return symbol;
    }
}
