package com.aoc.day11;

public class ComplexNeighbourFinder {
    public static int findNeighboursWithState(SeatStatus[][] seats, SeatStatus state, int i, int j) {
        int count = 0;
        int rows = seats.length;
        int columns = seats[0].length;
        int offset = 1;

        // NV neighbour
        while (i - offset >= 0 && j - offset >= 0) {
            if (seats[i - offset][j - offset] == state) {
                count++;
                break;
            } else if(seats[i - offset][j - offset] != SeatStatus.FLOOR) {
                break;
            }
            offset++;
        }

        // N neighbour
        offset = 1;
        while (i - offset >= 0) {
            if(seats[i-offset][j] == state) {
                count ++;
                break;
            } else if(seats[i-offset][j] != SeatStatus.FLOOR) {
                break;
            }
            offset++;
        }
        // NE neighbour
        offset = 1;
        while(i - offset >= 0 && j < columns - offset) {
            if(seats[i-offset][j+offset] == state) {
                count ++;
                break;
            } else if(seats[i-offset][j+offset] != SeatStatus.FLOOR) {
                break;
            }
            offset++;
        }

        // E neighbour
        offset = 1;
        while (j < columns - offset) {
            if(seats[i][j+offset] == state) {
                count ++;
                break;
            } else if(seats[i][j+offset] != SeatStatus.FLOOR) {
                break;
            }
            offset++;
        }
        // SE neighbour
        offset = 1;
        while (i < rows - offset && j < columns - offset) {
            if(seats[i+offset][j+offset] == state) {
                count ++;
                break;
            } else if(seats[i+offset][j+offset] != SeatStatus.FLOOR) {
                break;
            }
            offset++;
        }
        // S neighbour
        offset = 1;
        while(i < rows - offset) {
            if(seats[i+offset][j] == state) {
                count ++;
                break;
            } else if(seats[i+offset][j] != SeatStatus.FLOOR) {
                break;
            }
            offset++;
        }
        // SV neighbour
        offset = 1;
        while(i < rows - offset && j - offset >= 0) {
            if(seats[i+offset][j-offset] == state) {
                count ++;
                break;
            } else if(seats[i+offset][j-offset] != SeatStatus.FLOOR) {
                break;
            }
            offset++;
        }
        // V neighbour
        offset = 1;
        while(j - offset >= 0) {
            if(seats[i][j-offset] == state) {
                count ++;
                break;
            } else if(seats[i][j-offset] != SeatStatus.FLOOR) {
                break;
            }
            offset++;
        }
        return count;
    }
}
