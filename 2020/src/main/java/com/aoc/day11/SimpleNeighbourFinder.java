package com.aoc.day11;

public class SimpleNeighbourFinder {
    public static int findNeighboursWithState(SeatStatus[][] seats, SeatStatus state, int i, int j) {
        int count = 0;
        int rows = seats.length;
        int columns = seats[0].length;

        // NV neighbour
        if(i>0 && j > 0) {
            if(seats[i-1][j-1] == state) {
                count ++;
            }
        }

        // N neighbour
        if(i>0) {
            if(seats[i-1][j] == state) {
                count ++;
            }
        }
        // NE neighbour
        if(i>0 && j < columns - 1) {
            if(seats[i-1][j+1] == state) {
                count ++;
            }
        }

        // E neighbour
        if(j < columns - 1) {
            if(seats[i][j+1] == state) {
                count ++;
            }
        }
        // SE neighbour
        if(i < rows - 1 && j < columns - 1) {
            if(seats[i+1][j+1] == state) {
                count ++;
            }
        }
        // S neighbour
        if(i < rows - 1) {
            if(seats[i+1][j] == state) {
                count ++;
            }
        }
        // SV neighbour
        if(i < rows - 1 && j > 0) {
            if(seats[i+1][j-1] == state) {
                count ++;
            }
        }
        // V neighbour
        // N neighbour
        if(j>0) {
            if(seats[i][j-1] == state) {
                count ++;
            }
        }
        return count;
    }
}
