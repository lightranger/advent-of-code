package com.aoc.day11;

import java.io.IOException;

import static com.aoc.day11.SeatStatus.EMPTY;
import static com.aoc.day11.SeatStatus.OCCUPIED;

public class Seats {
    static boolean changed = false;

    public void solve() throws IOException {

        SeatsInput seatsInput = new SeatsInput();
        SeatStatus[][] seats = seatsInput.getSeats();
        //print(seats);
        do {
            changed = false;
            seats = processSeats(seats);
        } while (changed);

        System.out.println("Final: ");
        //print(seats);

        System.out.println("Occupied seats count: " + getOccupiedSeatsCount(seats));

    }

    private SeatStatus[][]  processSeats(SeatStatus[][] seats) {

        SeatStatus[][] newSeats = new SeatStatus[seats.length][seats[0].length];

        for(int i = 0; i < seats.length; i++) {
            for(int j = 0; j < seats[0].length; j++) {
                //System.out.print(seats[i][j].getSymbol()+" ");
                int occupiedSeats = ComplexNeighbourFinder.findNeighboursWithState(seats, OCCUPIED, i ,j);
                //int freeSeats = SimpleNeighbourFinder.findNeighboursWithState(seats, EMPTY, i ,j);

                switch (seats[i][j]) {
                    case OCCUPIED -> {
                        if(occupiedSeats >= 5) {
                            changed = true;
                            newSeats[i][j] = EMPTY;
                        } else {
                            newSeats[i][j] = seats[i][j];
                        }
                    }
                    case EMPTY -> {
                        if(occupiedSeats == 0) {
                            changed = true;
                            newSeats[i][j] = OCCUPIED;
                        } else {
                            newSeats[i][j] = seats[i][j];
                        }
                    }
                    case FLOOR -> {
                        newSeats[i][j] = seats[i][j];
                    }
                }
            }
        }
        //print(newSeats);
        return newSeats;
    }



    private void print(SeatStatus[][] seats) {
        for(int i = 0; i < seats.length; i++) {
            for(int j = 0; j < seats[0].length; j++) {
                System.out.print(seats[i][j].getSymbol());
            }
            System.out.println();
        }
        System.out.println();
    }

    private int getOccupiedSeatsCount(SeatStatus[][] seats) {
        int count = 0;
        for(int i = 0; i < seats.length; i++) {
            for(int j = 0; j < seats[0].length; j++) {
                if(seats[i][j] == OCCUPIED) {
                    count++;
                }
            }
        }
        return count;
    }

}
