package com.example.aoc

import com.example.aoc.day23.SafeCrack
import org.hamcrest.core.Is
import org.junit.Assert
import org.junit.Test

class SafeCrackTest {
    @Test fun test() {
        Assert.assertThat(SafeCrack().calculate(), Is.`is`(11748))
    }

    @Test fun testPart2() {
        Assert.assertThat(SafeCrack().calculateP2(), Is.`is`(479008308))
    }
}