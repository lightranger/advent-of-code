package com.example.aoc

import com.example.aoc.day3.SquareThreeSides
import org.hamcrest.core.Is.`is`
import org.junit.Assert
import org.junit.Test

class SquareThreeSidesTest {
    @Test fun test() {
        Assert.assertThat(SquareThreeSides().checkTriangles(),`is`(983))

    }

    @Test fun test2() {
        Assert.assertThat(SquareThreeSides().checkVerticalTriangles(),`is`(1836))
    }
}