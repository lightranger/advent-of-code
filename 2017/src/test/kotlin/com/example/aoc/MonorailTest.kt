package com.example.aoc

import com.example.aoc.day12.Monorail
import org.hamcrest.core.Is
import org.junit.Assert
import org.junit.Test

class MonorailTest {
    @Test fun test() {
        Assert.assertThat(Monorail().calculate(), Is.`is`(9227657)) // remove initialisation of registry c from monorail.in and the answer should be 318003 for part I
    }
}