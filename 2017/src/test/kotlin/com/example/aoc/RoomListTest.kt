package com.example.aoc

import com.example.aoc.day4.RoomList
import org.hamcrest.core.Is
import org.junit.Assert
import org.junit.Test

class RoomListTest {

    @Test fun test() {
        Assert.assertThat(RoomList().checkRooms(), Is.`is`(278221))

    }
}