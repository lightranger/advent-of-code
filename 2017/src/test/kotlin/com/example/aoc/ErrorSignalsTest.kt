package com.example.aoc

import com.example.aoc.day6.ErrorSignals
import org.hamcrest.core.Is
import org.junit.Assert
import org.junit.Test

class ErrorSignalsTest {
    @Test fun testMin() {
        Assert.assertThat(ErrorSignals().getErrorCorrectedMessageMin(), Is.`is`("aovueakv"))
    }

    @Test fun testMax() {
        Assert.assertThat(ErrorSignals().getErrorCorrectedMessageMax(), Is.`is`("umejzgdw"))
    }
}