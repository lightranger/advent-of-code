package com.example.aoc

import com.example.aoc.day9.Cyberspace
import org.hamcrest.core.Is
import org.junit.Assert
import org.junit.Test

class CyberspaceTest {
    @Test fun test() {
        Assert.assertThat(Cyberspace().getDecompressedLength(), Is.`is`(98135L)) // 10964557606L for part 2
    }
}