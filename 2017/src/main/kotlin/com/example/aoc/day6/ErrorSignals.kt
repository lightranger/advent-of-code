package com.example.aoc.day6

import java.io.File

class ErrorSignals {
    private val fileName: String = "/errorSignals.in"

    fun getErrorCorrectedMessageMin() : String {
        return getErrorCorrectedMessage(this::findCharacterMin)
    }

    fun getErrorCorrectedMessageMax() : String {
        return getErrorCorrectedMessage(this::findCharacterMax)
    }

    fun getErrorCorrectedMessage(fct : (m : List<String>, n : Int) -> Char?): String {
        val inputTextPath = ErrorSignals::class.java.getResource(fileName).path
        val readLines = File(inputTextPath).readLines()
        var i = 0
        val correctedMessage : StringBuilder = StringBuilder()

        while (i < readLines[0].length) {
            val soughtCharacter = fct(readLines, i++)
            correctedMessage.append(soughtCharacter)
        }

        return correctedMessage.toString()
    }

    private fun findCharacterMin(readLines: List<String>, i: Int): Char? {
        return readLines
                .groupBy { it[i] }
                .minBy { it.value.size }
                ?.key
    }

    private fun findCharacterMax(readLines: List<String>, i: Int): Char? {
        return readLines
                .groupBy { it[i] }
                .maxBy { it.value.size }
                ?.key
    }


}