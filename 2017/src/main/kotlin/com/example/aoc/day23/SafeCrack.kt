package com.example.aoc.day23

import com.example.aoc.day12.InstructionsProcessor
import java.io.File

class SafeCrack {
    private val fileName: String = "/safeCrack.in"
    private val fileNameP2: String = "/safeCrackPart2.in"

    fun calculate() : Int {
        val inputTextPath = SafeCrack::class.java.getResource(fileName).path
        val readLines = File(inputTextPath).readLines()
        val instructionsProcessor = InstructionsProcessor(readLines.toMutableList())
        return instructionsProcessor.process()
    }

    fun calculateP2() : Int {
        val inputTextPath = SafeCrack::class.java.getResource(fileNameP2).path
        val readLines = File(inputTextPath).readLines()
        val instructionsProcessor = InstructionsProcessor(readLines.toMutableList())
        return instructionsProcessor.process()
    }
}