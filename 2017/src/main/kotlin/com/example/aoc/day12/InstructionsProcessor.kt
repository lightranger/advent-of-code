package com.example.aoc.day12

class InstructionsProcessor(val instructions: MutableList<String>) {

    val instructionProcessor: InstructionProcessor
    var currentInstructionIndex = 0

    init {
        instructionProcessor = InstructionProcessor(this::jumpToIndex, this::toggleLine)
    }

    fun process(): Int {
        while (currentInstructionIndex < instructions.size) {
            instructionProcessor.process(instructions[currentInstructionIndex])
            ++currentInstructionIndex
        }
        return instructionProcessor.getRegistryEntry("a")
    }

    fun jumpToIndex(indexOffset: Int) {
        currentInstructionIndex += indexOffset - 1
    }

    fun toggleLine(indexOffset: Int) {
        val indexToToggle = currentInstructionIndex + indexOffset
        if (indexToToggle < instructions.size && indexToToggle >= 0) {
            val instructionToBeToggled = instructions[indexToToggle]
            instructions[indexToToggle] = instructionProcessor.toggleLine(instructionToBeToggled)
        }
    }
}