package com.example.aoc.day12

import java.io.File

class Monorail {
    private val fileName: String = "/monorail.in"

    fun calculate() : Int {
        val inputTextPath = Monorail::class.java.getResource(fileName).path
        val readLines = File(inputTextPath).readLines()
        val instructionsProcessor = InstructionsProcessor(readLines.toMutableList())
        return instructionsProcessor.process()
    }
}