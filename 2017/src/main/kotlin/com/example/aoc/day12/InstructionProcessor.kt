package com.example.aoc.day12

import java.util.*


class InstructionProcessor(val indexOffsetModifier: (i: Int) -> Unit, val toggleLineFct: (i: Int) -> Unit) {

    private val INSTRUCTION_INDEX = 0
    private val SOURCE_INDEX = 1
    private val DESTINATION_INDEX = 2
    private val registry = HashMap<String, Int>()
    private val SEPARATOR_CHARACTER = " "

    fun process(instruction: String) {
        val split = instruction.split(SEPARATOR_CHARACTER)


        when (split[INSTRUCTION_INDEX]) {
            "cpy" -> copy(split[SOURCE_INDEX], split[DESTINATION_INDEX])
            "inc" -> inc(split[SOURCE_INDEX])
            "dec" -> dec(split[SOURCE_INDEX])
            "jnz" -> jnz(split[SOURCE_INDEX], split[DESTINATION_INDEX])
            "tgl" -> tgl(split[SOURCE_INDEX])
            "mul" -> mul(split[SOURCE_INDEX], split[DESTINATION_INDEX])
        }
    }

    fun toggleLine(instructionToBeToggled : String) : String{
        val split = instructionToBeToggled.split(SEPARATOR_CHARACTER)

        when(split.size - 1) {
            1 -> return "${getOneArgumentToggle(split[0])} ${split[1]}"
            2 -> return "${getTwoArgumentToggle(split[0])} ${split[1]} ${split[2]}"
        }
        throw IllegalArgumentException()
    }

    fun getOneArgumentToggle(instruction : String) : String {
        when(instruction) {
            "inc" -> return "dec"
            else -> return "inc"
        }
    }

    fun getTwoArgumentToggle(instruction : String) : String {
        when(instruction) {
            "jnz" -> return "cpy"
            else -> return "jnz"
        }
    }


    fun getRegistryEntry(registryName : String) : Int {
        return registry[registryName] ?: throw IllegalStateException("No value in the register")
    }

    private fun copy(xSource: String, yDestination: String) {
        val value = registry[xSource] ?: xSource.toInt()
        registry.put(yDestination, value)
    }

    private fun mul(xSource: String, yDestination: String) {
        val sourceValue = registry[xSource] ?: xSource.toInt()
        val destinationValue = registry[yDestination] ?: xSource.toInt()
        registry.put(yDestination, sourceValue * destinationValue)
    }

    private fun inc(xSource: String) {
        val value = registry[xSource] ?: throw IllegalArgumentException("Source expected")
        registry.put(xSource, value + 1)
    }

    private fun dec(xSource: String) {
        val value = registry[xSource]
        if( value != null)
            registry.put(xSource, value - 1)
    }

    private fun jnz(xSource: String, yOffset: String) {
        val value = registry[xSource] ?: xSource.toIntOrNull() ?: 0
        val yOffsetN = registry[yOffset] ?: yOffset.toIntOrNull()
        if (value != 0 && yOffsetN != null) {
            indexOffsetModifier(yOffsetN)
        }
    }

    private fun tgl(xSource: String) {
        val value = registry[xSource] ?: xSource.toIntOrNull() ?: 0
        toggleLineFct(value)
    }
}
