package com.example.aoc.day9

import java.io.File

class Cyberspace {
    companion object {
        private val fileName: String = "/cyberspace.in"
    }

    fun getDecompressedLength() : Long {
        val inputTextPath = Cyberspace::class.java.getResource(fileName).path
        val input = File(inputTextPath).readLines()[0]

        return getDecompressed(input)
    }

    fun getDecompressed(input : String) : Long{
        var newInputSize = 0L
        var currentIndex = -1
        var markerBracketBegin = 0
        var isBuildingMarker = false

        while(++currentIndex < input.length ) when (input[currentIndex]) {
            '(' -> {
                markerBracketBegin = currentIndex
                isBuildingMarker = true
            }
            ')' -> {
                val marker = input.substring(markerBracketBegin+1, currentIndex)
                val split = marker.split("x")
                val length = split[0].toInt()
                val repeatTimes = split[1].toInt()
                //newInputSize += repeatTimes * getDecompressed(input.substring(currentIndex+1, currentIndex+length+1))// part 2
                newInputSize += repeatTimes * input.substring(currentIndex+1, currentIndex+length+1).length // part 1
                currentIndex += length
                isBuildingMarker = false
            }
            else  -> {
                if(!isBuildingMarker) {
                    newInputSize++
                }
            }
        }
        return newInputSize
    }
}