package com.example.aoc.day3

import java.io.File


class SquareThreeSides {
    private val fileName = "/squareThreeSides.in"

    fun checkTriangles() : Int{
        val inputTextPat = SquareThreeSides::class.java.getResource(fileName).path
        val useLines = File(inputTextPat)
                .useLines { it -> it
                        .map { it -> getTriangle(it) }
                        .filter { it -> isValidTriangle(it) }
                        .count()
                }
        return useLines
    }

    fun checkVerticalTriangles() : Int{
        val inputTextPat = SquareThreeSides::class.java.getResource(fileName).path
        val useLines = File(inputTextPat)
                .useLines { it -> it
                        .map { it -> getTriangle(it) }
                        .withIndex().groupBy { it.index / 3 }
                        .values.map { it -> getValidTrianglesNo(it) }
                        .sum()
                }
        return useLines
    }

    private fun getValidTrianglesNo(triangles : List<IndexedValue<Triangle>> ) : Int{
        val list = mutableListOf<Boolean>()
        list.add(areNumbersValidTrianglesSides(triangles[0].value.l1, triangles[1].value.l1, triangles[2].value.l1))
        list.add(areNumbersValidTrianglesSides(triangles[0].value.l2, triangles[1].value.l2, triangles[2].value.l2))
        list.add(areNumbersValidTrianglesSides(triangles[0].value.l3, triangles[1].value.l3, triangles[2].value.l3))
        return list.filter { it }.count()

    }

    private fun areNumbersValidTrianglesSides(l1:Int, l2:Int, l3:Int) : Boolean = l2 + l1 > l3 && l1 + l3 > l2 && l2 + l3 > l1

    private fun isValidTriangle(triangle: Triangle) : Boolean = triangle.l1 + triangle.l2 > triangle.l3 &&
            triangle.l1 + triangle.l3 > triangle.l2 &&
            triangle.l2 + triangle.l3 > triangle.l1


    private fun getTriangle(line : String) : Triangle {
        val splitToSequence = line.split(" ").filter { it.isNotBlank() }.map { it -> Integer.parseInt(it) }
        return Triangle(splitToSequence[0], splitToSequence[1], splitToSequence[2])
    }
}