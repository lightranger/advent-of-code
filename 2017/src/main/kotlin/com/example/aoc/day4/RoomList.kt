package com.example.aoc.day4

import java.io.File

class RoomList {
    private val fileName : String = "/roomList.in"

    fun checkRooms() : Int{
        val inputTextPath = RoomList::class.java.getResource(fileName).path
        val roomProcessor = RoomProcessor()

        val sectorIdSum = File(inputTextPath)
                .useLines {
                    it ->
                    it.map { it -> roomProcessor.processLineGetSectorId(it) }
                            .sum()
                }
        return sectorIdSum
    }


}