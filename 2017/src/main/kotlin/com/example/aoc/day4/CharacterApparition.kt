package com.example.aoc.day4

data class CharacterApparition (val character: Char, val apparitions: Int = 0) : Comparable<CharacterApparition>{
    override fun compareTo(other: CharacterApparition): Int /*{
        val compareTo = apparitions.compareTo(other.apparitions)
        if(compareTo == 0) {
            return character.compareTo(other.character)
        }
        return -compareTo
    }*/
     = compareValuesBy(this, other, {-it.apparitions}, {it.character})


    override fun hashCode(): Int {
        return character.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        if( this === other)
            return true;
        if(other?.javaClass != javaClass)
            return false;

        other as CharacterApparition

        return this.character == other.character
    }
}