package com.example.aoc.day4

class RoomProcessor {
    private val REGEX_PATTERN: java.util.regex.Pattern = java.util.regex.Pattern.compile(com.example.aoc.day4.RoomProcessor.Companion.REGEX_PATTERN_VALUE)

    private var i: Int = 0

    companion object {
        private const val REGEX_PATTERN_VALUE = "(\\d*)\\[(.*)]"
    }

    fun processLineGetSectorId(line: String): Int {
        val (characterApparitionCountMap, sectorIdCheckSumPair) = processLine(line)
        val actualChecksum = getActualChecksum(characterApparitionCountMap)
        val decrypted = getDecryptedMessage(line, sectorIdCheckSumPair)

        println((++i).toString() + " " + decrypted + " " + sectorIdCheckSumPair.first)

        if (actualChecksum.startsWith(sectorIdCheckSumPair.second)) {
            return sectorIdCheckSumPair.first
        }

        return 0
    }

    private fun processLine(line: String): Pair<Map<Char, Int>, Pair<Int, String>> {
        val map = java.util.HashMap<Char, Int>()

        val encryptedNames = line.splitToSequence("-").toMutableList()
        val lastElement = encryptedNames.last()
        encryptedNames.removeAt(encryptedNames.size - 1)

        encryptedNames.forEach {
            it.forEach { it ->
                updateApparitionCount(map, it)
            }
        }
        return Pair(map, getSectorIdCheckSumPair(lastElement))
    }

    private fun updateApparitionCount(map: java.util.HashMap<Char, Int>, it: Char) {
        val apparitionCount = map.getOrDefault(it, 0)
        if (apparitionCount == 0) {
            map.put(it, 1)
        }
        map.put(it, apparitionCount + 1)
    }

    private fun getSectorIdCheckSumPair(s: String): Pair<Int, String> {
        val m: java.util.regex.Matcher = REGEX_PATTERN.matcher(s)
        m.find()
        val sectorId = m.group(1)
        val verif = m.group(2)
        return Pair(Integer.valueOf(sectorId), verif)
    }

    private fun getActualChecksum(map: Map<Char, Int>): String {
        val tree = java.util.TreeSet<CharacterApparition>()
        for ((key, value) in map) {
            tree.add(CharacterApparition(key, value))
        }

        return tree.joinToString("") { it.character.toString() }
    }

    private fun getDecryptedMessage(line: String, sectorIdCheckSumPair: Pair<Int, String>) = line.toCharArray().map { it -> getCharValue(it, sectorIdCheckSumPair.first) }.joinToString("")


    private fun getCharValue(ch: Char, times: Int): Char {
        when (ch) {
            '-' -> return ' '
            else -> return rotateCharValue(ch, times)
        }
    }

    private fun rotateCharValue(ch: Char, times: Int): Char {
        val zaDistance = 'z' - 'a' + 1
        return 'a' + ((ch - 'a' + times) % zaDistance)
    }

}